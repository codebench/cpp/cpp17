#include <iostream>

int foo(bool a) {
  return 3;
  // return 30;
}
int main() {
auto a = false;
auto c = 10;
// auto b = foo(a);
// if (!a && (b < c))
 if (auto b = foo(a); (!a && (b < c)))
{
  std::cout << "First if() == true" << std::endl;
}

 if (int b; (!a && ((b = foo(a) ) < c)))
{
  std::cout << "Second if() == true" << std::endl;
}

}
